{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Model where

import Control.Applicative ((<$>))
import Control.Monad.Reader (ask)
import Control.Monad.State (get, put)
import Data.Acid (Query, Update, makeAcidic)
import Data.Aeson (FromJSON(parseJSON), ToJSON, withObject, (.:?))
import Data.IntMap (IntMap, delete, insert, elems, lookup, empty)
import Data.List (sortBy)
import Data.Ord (comparing)
import Data.SafeCopy (deriveSafeCopy, base)
import Data.Text (Text, pack, append)
import Data.Typeable (Typeable)
import GHC.Generics (Generic)
import Prelude hiding (lookup)


data TodoRq = TodoRq
  { rqTitle :: Maybe Text
  , rqOrder :: Maybe Int
  , rqCompleted :: Maybe Bool
  } deriving (Show, Generic)

data Todo = Todo
  { title ::Text
  , order :: Int
  , completed :: Bool
  , url :: Text
  , key :: Int
  } deriving (Show, Generic, Typeable)

data TodoDb = TodoDb
  { allTodos :: IntMap Todo
  , nextKey :: Int
  , baseURL :: Text
  } deriving (Show, Typeable)


instance FromJSON TodoRq where
  parseJSON = withObject "TodoRq" $ \o -> do
    rqTitle <- o .:? "title"
    rqOrder <- o .:? "order"
    rqCompleted <- o .:? "completed"
    return TodoRq{..}

instance ToJSON Todo

initialTodoDbState :: Text -> TodoDb
initialTodoDbState url =
  TodoDb { allTodos = empty
         , nextKey = 1
         , baseURL = url
         }

peekNextKey :: Query TodoDb Int
peekNextKey = nextKey <$> ask

incNextKey :: Update TodoDb ()
incNextKey = do
  t@TodoDb{..} <- get
  let newNextKey = nextKey + 1
  put $ t { nextKey = newNextKey }

addTodo :: Todo -> Update TodoDb ()
addTodo todo = do
  t@TodoDb{..} <- get
  let newTodo = todo { key = nextKey
                     , url = append baseURL $ pack $ show nextKey 
                     }
      newAllTodos = insert nextKey newTodo allTodos
      newNextKey = nextKey + 1
  put $ t { allTodos = newAllTodos, nextKey = newNextKey }

deleteAllTodos :: Update TodoDb ()
deleteAllTodos = do
  t@TodoDb{..} <- get
  put $ t { allTodos = empty, nextKey = 1 }

deleteTodoByKey :: Int -> Update TodoDb ()
deleteTodoByKey key = do
  t@TodoDb{..} <- get
  let newAllTodos = delete key allTodos
  put $ t { allTodos = newAllTodos } 

todoByKey :: Int -> Query TodoDb (Maybe Todo)
todoByKey key = do 
  TodoDb{..} <- ask
  return $ lookup key allTodos

todosByOrder :: Query TodoDb [Todo]
todosByOrder =
  sortBy (comparing order) . elems . allTodos <$> ask

deriveSafeCopy 0 'base ''Todo
deriveSafeCopy 0 'base ''TodoDb
makeAcidic ''TodoDb ['addTodo, 'todosByOrder, 'todoByKey, 'deleteTodoByKey, 'deleteAllTodos, 'peekNextKey, 'incNextKey]
