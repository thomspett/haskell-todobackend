{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Main where

import Control.Applicative ( (<$>) )
import Control.Monad (msum, mzero)
import Control.Monad.Reader (ask)
import Control.Monad.State (modify, get, put)
import Control.Monad.Trans (liftIO)
import Data.Acid
import Data.Acid.Advanced (update', query')
import Data.Acid.Memory
import Data.Aeson ((.:?))
import Data.Data
import Data.IntMap (IntMap)
import Data.Maybe (fromJust, fromMaybe)
import Data.SafeCopy
import GHC.Generics
import Happstack.Server
import Happstack.Server.Types 
import System.Console.CmdArgs.Implicit ((&=))
import System.IO
import System.Log.Logger (Priority(..), updateGlobalLogger, setLevel, rootLoggerName )
import System.Log.Handler.Simple

import qualified Happstack.Server as H (timeout, port)
import qualified Data.Aeson                       as A
import qualified Data.IntMap                      as IntMap
import qualified Data.ByteString.Lazy.Char8       as L
import qualified Data.Text                        as T
import qualified System.Console.CmdArgs.Implicit  as I

import Model hiding (addTodo, deleteAllTodos, deleteTodoByKey)

mkTodo :: TodoRq -> Todo
mkTodo TodoRq{..} = Todo 
  { title = fromMaybe "" rqTitle
  , completed = fromMaybe False rqCompleted
  , order = fromMaybe 0 rqOrder
  , url = ""
  , key = 0
  }

onlyHeaders =
  ok (toResponse ("[]"::L.ByteString)) { rsHeaders = corsHeaders }

getAllTodos acid = do
  todos <- query' acid TodosByOrder
  ok (toResponse (A.encode todos)) { rsHeaders = corsHeaders }

getTodoByKey acid key = do
  todo <- query' acid (TodoByKey key)
  ok ( toResponse ( A.encode todo )) { rsHeaders = corsHeaders }

deleteTodoByKey acid key = do
  update' acid (DeleteTodoByKey key)
  todo <- query' acid (TodoByKey key)
  ok ( toResponse ( A.encode todo )) { rsHeaders = corsHeaders }

deleteAllTodos acid = do 
  update' acid DeleteAllTodos
  todos <- query' acid TodosByOrder
  ok (toResponse (A.encode todos)) { rsHeaders = corsHeaders }

addTodo acid = do
  body <- getBody
  let todoRq = fromJust $ A.decode body :: TodoRq
  key <- query' acid PeekNextKey
  update' acid (AddTodo (mkTodo todoRq))
  todo <- query' acid (TodoByKey key)
  ok (toResponse $ A.encode todo) { rsHeaders = corsHeaders }

corsHeaders = mkHeaders 
      [ ("Access-Control-Allow-Origin","*")
      , ("Access-Control-Allow-Headers","Accept, Content-Type")
      , ("Access-Control-Allow-Methods","PATCH, GET, HEAD, POST, PUT, DELETE, OPTIONS")
      , ("Content-Type","application/json")
      ]


getBody :: ServerPart L.ByteString
getBody = do
  req <- askRq
  body <- liftIO $ takeRequestBody req
  case body of
    Just rqBody -> return . unBody $ rqBody
    Nothing -> return ""

main :: IO ()
main = do
  updateGlobalLogger "Happstack" (setLevel INFO)
  config <- I.cmdArgs aConfig
  let baseURL = confURL config
  state <- openMemoryState $ initialTodoDbState baseURL
  simpleHTTP (hConf config) $ handlers state

handlers :: AcidState TodoDb -> ServerPart Response
handlers acid = msum
      [ do method OPTIONS 
           onlyHeaders
      , do dir "todos" nullDir  
           do method POST
              addTodo acid
      , do dir "todos" nullDir 
           do method GET 
              getAllTodos acid
      , do dir "todos" nullDir 
           do method DELETE 
              deleteAllTodos acid
      , do method GET 
           dir "todos" $ path $ \i -> getTodoByKey acid (i::Int)
      , do method DELETE 
           dir "todos" $ path $ \i -> deleteTodoByKey acid (i::Int)
      , onlyHeaders
      ]

-- Config
--

data Config =
  Config { port :: Int, timeout :: Int, url :: T.Text } deriving (Show, Eq, Data, Typeable )

hConf :: Config -> Conf
hConf (Config {..}) = nullConf { H.timeout = timeout, H.port = port }

aConfig :: Config
aConfig =
  Config { port    = 8000  &= I.help "Port number"
                           &= I.typ "INT"
         , timeout = 30    &= I.help "Timeout"
                           &= I.typ "SECONDS"
         , url = "http://127.0.0.1:8000/todos" &= I.help "Base URL"
                                         &= I.typ "STRING"
         }
    &= I.summary "Heroku-Test server"
    &= I.program "heroku-test"

